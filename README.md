Cordova Tutorial Windows Installation
========

! Ingat disini kita selalu menggunakan terminal bawaan OS untuk mengetikkan perintah-perintah dibawah, karena ini tutorial windows kita akan menggunakan yang namanya `Command Prompt / CMD` cara menjalankan CMD dengan menekan tombol `Windows` lalu ketik `'Command Prompt'`

# Persiapan
- Software yang perlu Diinstall
    1. NodeJs - [Cara Install](https://www.codepolitan.com/cara-install-nodejs) || [Download](https://nodejs.org/en/download/)
    2. OpenJDK - [Cara Install](https://www.kodingindonesia.com/cara-install-jdk/) || [Download]()
    3. Gradle - [Cara Install]() || [Download](https://gradle.org/releases/)
    4. Android Studio + SDK - [Cara Install](https://www.dicoding.com/blog/cara-install-android-studio-dalam-15-menit/) || [Download](https://developer.android.com/studio/index.html)

Disini saya tidak akan menjelaskan lebih dalam tentang cara install software-software diatas, tapi saya taruh link cara install diatas.
</br>

# Instalasi Cordova
Sebelum pergi ke step selanjutnya, check apakah software lainnya sudah terinstall dengan mengetikkan perintah-perintah berikut lewat terminal:

NodeJs 
```
node -v
```
Java Development Kit (JDK)
```
java -version
```

Jika NodeJs sudah diinstall ketikkan perintah berikut:
```
npm install -g cordova
```

Instalasi membutuhkan koneksi internet. Tunggu proses instalasi dan bila telah selesai, tes hasil instalasi untuk mengecek apakah cordova sudah terinstal dengan perintah berikut:

```
cordova -v
```

Jika muncul angka-angka atau gambar seperti dibawah ini berarti cordova sudah terinstal.
![Code](image/Capture.PNG)

setelah itu kita akan buat project dengan mengetikkan perintah seperti dibawah ini:

```
cordova create namaProject
```

contoh:

![Code](image/createCordova.PNG)

Jika project sudah terbuat maka akan muncul file html halaman utama seperti ini:

![Code](/image/full-proj.png)

Karena JDK, Gradle, dan Android SDK sudah kita siapkan.. maka kita bisa membuat APK dan juga menjalankannya di emulator.

Tapi sebelum itu kita harus tambahkan dulu platformnya.

Silahkan ketik perintah:

```
cordova platform add android
```

Kemudian, jika ingin menjalankan di emulator ketik perintah:

```
cordova run android
```

<!-- Hasilnya akan seperti ini:

![]() -->

Lalu jika ingin membuat file APK-nya, kita bisa ketik:

```
cordova build android --release
```
Atau jika ingin buat APK untuk dipublish ke Playstore, ketik:

```
cordova build android --release -- --keystore="..\android.keystore" --storePassword=android --alias=mykey
```